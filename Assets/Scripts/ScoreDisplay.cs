﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour {

    public GameObject[] frameObjects;

    private SortedDictionary<int,Text[]> bowlTextList = new SortedDictionary<int, Text[]>();
    private SortedDictionary<int, Text> frameTextList = new SortedDictionary<int, Text>();


    private string scoreADisplayText = "";
    private string scoreBDisplayText = "";
    private string scoreCDisplayText = "";

    private int scoreA = 0;
    private int scoreB = 0;
    private int scoreC = 0;


    // Use this for initialization
    void Start () {

        InitializeDisplay();
        
        
	}

    /*
     * Dynamically gathers Text elements from the UI and places them
     * in respective arrays of individual bowlscores and framescores
     * */
    public void InitializeDisplay()
    {
        int index = 1;
        foreach (GameObject frameObject in frameObjects)
        {
            int bowlTextObjectIndex = 0;
            Text[] bowlTextArray = new Text[3];
            foreach (Transform textObject in frameObject.transform)
            {
                if (textObject.tag == "BowlA" || textObject.tag == "BowlB" || textObject.tag == "BowlC")
                {

                    Text bowlTextObject = textObject.GetComponent<Text>();
                    bowlTextArray[bowlTextObjectIndex] = bowlTextObject;
                    try
                    {
                        bowlTextList.Add(index, bowlTextArray);
                    }
                    catch (ArgumentException)
                    {
                        bowlTextList[index] = bowlTextArray;
                    }
                    bowlTextObjectIndex++;
                }
                else
                {
                    Text frameTextObject = textObject.GetComponent<Text>();
                    try
                    {
                        frameTextList.Add(index, frameTextObject);
                    }
                    catch (ArgumentException)
                    {
                        frameTextList[index] = frameTextObject;
                    }
                    }
            }
            index++;
        }
    }

    public void ResetDisplay()
    {
        foreach(Text[] bowlText in bowlTextList.Values)
        {
            for (int i = 0; i < bowlText.Length; i++)
            {
                bowlText[i].text = "0";
            }
        }

        foreach(Text frameText in frameTextList.Values)
        {
            frameText.text = "0";
        }
    }



   

    public void HandleIndividualBowls(int frameIndex,List<int> individScores)
    {
        ExtractIndividualBowlScores(individScores);
        DecideOnDisplayedBowlScores(frameIndex);  
        
    }

    

    private void DecideOnDisplayedBowlScores(int frameIndex)
    {
        scoreADisplayText = scoreA.ToString();
        scoreBDisplayText = scoreB.ToString();
        scoreCDisplayText = scoreC.ToString();


        if (frameIndex == 10) { HandleFrame10(); }
        else
        {
            for (int i = 0; i < bowlTextList[frameIndex].Length; i++)
            {

                if (scoreA == 10)
                {
                    scoreADisplayText = "X";
                    scoreBDisplayText = "-";

                }
                else if (scoreA != 10 && (scoreA + scoreB) == 10)
                {
                    scoreBDisplayText = "/";
                }

                UpdateIndividualBowlDisplay(frameIndex, i);

            }
        }
        
    }
    // TODO handle last frame with special cases
    private void HandleFrame10()
    {
        if ((scoreA + scoreB) == 10 && scoreA != 10)
        {
            scoreBDisplayText = "/";
        } else if((scoreB+scoreC) == 10 && scoreB != 10)
        {
            scoreCDisplayText = "/";
        }
        if (scoreA == 10)
        {
            scoreADisplayText = "X";
        }
        if(scoreB == 10)
        {
            scoreBDisplayText = "X";
        }
        if(scoreC == 10)
        {
            scoreCDisplayText = "X";
        }

        for(int i=0; i<bowlTextList[10].Length; i++)
        {
            UpdateIndividualBowlDisplay(10, i);
        }
        
        

        

    }

    private void ExtractIndividualBowlScores(List<int> individScores)
    {
        for (int i = 0; i < individScores.Count; i++)
        {
            if (i == 0)
            {
                scoreA = individScores[i];
            }
            else if (i == 1)
            {
                scoreB = individScores[i];
            }
            else if (i == 2)
            {
                scoreC = individScores[i];
            }

        }
        
    }

    private void UpdateIndividualBowlDisplay(int frameIndex, int index)
    {
        if (index == 0)
        {
            bowlTextList[frameIndex][index].text = scoreADisplayText;
            bowlTextList[frameIndex][index].color = Color.white;
        }
        else if (index == 1)
        {
            bowlTextList[frameIndex][index].text = scoreBDisplayText;
            bowlTextList[frameIndex][index].color = Color.white;
            ResetBowlScoreValues();
            
        }
    }



    public void HandleFrameScore(int frameIndex, int frameScore)
    {
        string frameScoreValueToDisplay = frameScore.ToString();
        frameTextList[frameIndex-1].text = frameScoreValueToDisplay;
        frameTextList[frameIndex-1].color = Color.white;
    }



    private void ResetBowlScoreValues()
    {
        scoreADisplayText = "0";
        scoreBDisplayText = "0";
        scoreCDisplayText = "0";
        scoreA = 0;
        scoreB = 0;
        scoreC = 0;
    }


    /*
     * Method for UnitTesting, uses behaviours of the class but doesnt display
     * anything in the UI
     * */
    public string[] TestingBowls(int frameIndex, List<int> individScores)
    {
        ExtractIndividualBowlScores(individScores);
        scoreADisplayText = scoreA.ToString();
        scoreBDisplayText = scoreB.ToString();
        scoreCDisplayText = scoreC.ToString();

        string[] returnBowls;
        if (frameIndex != 10)
        {
            if ((scoreA + scoreB) == 10 && scoreA != 10)
            {
                scoreBDisplayText = "/";
            }
            else if (scoreA == 10)
            {
                scoreADisplayText = "X";
                scoreBDisplayText = "-";
            }
            returnBowls = new string[2];
            returnBowls[0] = scoreADisplayText;
            returnBowls[1] = scoreBDisplayText;
        }
        else
        {
            HandleFrame10();
            returnBowls = new string[3];
            returnBowls[0] = scoreADisplayText;
            returnBowls[1] = scoreBDisplayText;
            returnBowls[2] = scoreCDisplayText;
        }
        return returnBowls;
    }

}
