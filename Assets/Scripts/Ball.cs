﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Ball : MonoBehaviour {

    public Vector3 launchVelocity;
    public bool inPlay = false;

    private Rigidbody rigidBody;
    private AudioSource audio;
    
    private Vector3 resetPosition;

	// Use this for initialization
	void Start () {
        audio = GetComponent<AudioSource>();
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.useGravity = false;

        

        resetPosition = transform.position; // captures starting position
        
	}

       
    public void LaunchBall(Vector3 velocity)
    {
        
        rigidBody.useGravity = true;
        rigidBody.velocity = velocity;

        inPlay = true;
    }

    /**
     * Test launch method for use in editor. Simulates the perfect throw!
     * */
     public void SimulateLaunchBall()
    {
        LaunchBall(new Vector3(0f, 0f, 1550f));
    }

    public void Reset()
    {
        transform.position = resetPosition;
        transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
        rigidBody.useGravity = false;
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
        Debug.Log("Resetting ball");
        inPlay = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject collisionObj = (GameObject)collision.gameObject;
        if (collisionObj.tag == "Floor")
        {
            audio.Play();
        }
    }
}
