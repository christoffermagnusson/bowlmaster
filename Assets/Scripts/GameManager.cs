﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    // Scoring components
    private ScoreMaster scoreMaster;
    private SortedDictionary<int, int[]> scores = new SortedDictionary<int, int[]>();
    private static int currentFrame = 1;
    private List<int> currentFrameScore = new List<int>();
    private List<int> returnedScores = new List<int>();
    ScoreDisplay scoreDisplay;

    // Action components
    private ActionMaster actionMaster;
    

    
    // Pin handling components
    private PinSetter pinSetter;
    private PinCounter pinCounter;

    // General game components
    private Ball ball; 
    private CameraControl camera;
    private BallDragLaunch ballDragLaunch;


    // End game dialog
    private CanvasGroup endGameDialogCanvas;


    private void Start()
    {
        actionMaster = new ActionMaster();
        scoreMaster = new ScoreMaster();
        scoreDisplay = GameObject.FindObjectOfType<ScoreDisplay>();
        pinSetter = GameObject.FindObjectOfType<PinSetter>();
        pinCounter = GameObject.FindObjectOfType<PinCounter>();
        ball = GameObject.FindObjectOfType<Ball>();
        camera = GameObject.FindObjectOfType<CameraControl>();
        ballDragLaunch = GameObject.FindObjectOfType<BallDragLaunch>();
        GameObject endGameDialogObj = GameObject.Find("EndGameCanvasGroup");
        endGameDialogCanvas = endGameDialogObj.GetComponent<CanvasGroup>();
        
    }

    private void ShowEndGameDialogComponents()
    {
        endGameDialogCanvas.alpha = 1f;
        endGameDialogCanvas.interactable = true;
    }

    private void HideEndGameDialogComponents()
    {
        endGameDialogCanvas.alpha = 0f;
        endGameDialogCanvas.interactable = false;
    }

    public void ReceiveFallenPins(int pinsFallen)
    {
        
        HandleScoring(pinsFallen);
        DecideOnAction(actionMaster.Bowl(pinsFallen));
        ballDragLaunch.EnableArrowButtons(true);
    } 

    public void DecideOnAction(ActionMaster.Action action)
    {
        pinSetter.SetStandingPins(pinCounter.GetStandingPinArray());
        switch (action)
        {
            case ActionMaster.Action.Tidy:
                pinSetter.Tidy();
                break;
            case ActionMaster.Action.EndTurn:
                pinCounter.Reset();
                pinSetter.RenewPins();
                currentFrame++;
                currentFrameScore.Clear();
                break;
            case ActionMaster.Action.Reset:
                pinCounter.Reset();
                pinSetter.RenewPins();
                break;
            case ActionMaster.Action.EndGame:
                ShowEndGameDialogComponents();
                break;

        }
        

    }

    

    public void HandleEndGame()
    {
        pinCounter.Reset();
        pinSetter.RenewPins();
        currentFrame = 1;
        currentFrameScore.Clear();
        scoreDisplay.ResetDisplay();
        HideEndGameDialogComponents();
    }

    public void HandleQuitGame()
    {
        Application.Quit();
    }

    private void HandleScoring(int pinsFallen)
    {
        currentFrameScore.Add(pinsFallen);
        
        try
        {
            scores.Add(currentFrame, currentFrameScore.ToArray());
        } catch(ArgumentException)
        {
            scores[currentFrame] = currentFrameScore.ToArray();
        }
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, currentFrame);
            scoreDisplay.HandleFrameScore(scoreMaster.GetCurrentFrameToScore(), scoreMaster.GetRunningTotal());
        }
        
         
        scoreDisplay.HandleIndividualBowls(currentFrame,currentFrameScore);
        
        
    }

    public void ResetTurn()
    {
        ball.Reset();
        camera.Reset();
        pinCounter.UpdatePinCounterText(PinCounter.TextColor.Default); 
    }

    public static void SetCurrentBowl(int newFrame)
    {
        currentFrame = newFrame;
    }

    public static int GetCurrentBowl()
    {
        return currentFrame;
    }
}
