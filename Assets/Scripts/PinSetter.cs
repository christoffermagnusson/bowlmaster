﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinSetter : MonoBehaviour {

    
    
    public GameObject pinSet;
     
    private Animator animator;
    private GameObject pinParent;
    private GameManager gameManager;
    
    // to be set by gamemanager fetched from PinCounter
    private ArrayList standingPins = new ArrayList();
    

   
    void Start () {
        animator = GetComponent<Animator>();
        pinParent = GameObject.Find("Pins");
        gameManager = GameObject.FindObjectOfType<GameManager>();
	}

    

    public void RaisePins()
    { 
        foreach(Pin pin in standingPins)
        {
            if (pin != null)
            {
                pin.Raise();
            }
        }
    }

    public void LowerPins() 
    {
        gameManager.ResetTurn();
        foreach (Pin pin in standingPins)
        {
            if (pin != null)
            {
                pin.Lower();
            }
        }
         // makes sure that the game doesnt start until all pins have been lowered


    }

    public void SetStandingPins(ArrayList standingPins)
    {
        this.standingPins = standingPins;

    }

    public void RenewPins()
    {
        gameManager.ResetTurn();

        GameObject.Destroy(GameObject.FindGameObjectWithTag("PinParent"));
        
        GameObject newParent = (GameObject)Instantiate(pinSet);
        newParent.transform.position = new Vector3(-10f, 2f, 1829f);
        
        foreach (Transform child in newParent.transform)
        {
                Pin pin = child.GetComponent<Pin>();
                pin.GetComponent<Rigidbody>().isKinematic = true;
        }
        

        foreach(Transform child in newParent.transform)
        {
            Pin pin = child.GetComponent<Pin>();
            pin.GetComponent<Rigidbody>().isKinematic = false;
        }
        
        
        
    }

   

    

    

   
 


   

   
   

    /**
     * Intermediating methods activating stuff in the animator regarding 
     * starting animations when the player has finished with a throw or game
     * */
    public void Tidy()
    {
        // Raise
        // Swipe
        // Lower
        animator.SetTrigger("tidyTrigger");   
    }

    public void Reset()
    {
        // Raise
        // Renew
        // Lower is not included, caused some weird crashes
        animator.SetTrigger("resetTrigger");      
    }

    

    

  
    
}
