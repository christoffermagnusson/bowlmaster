﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent (typeof(Ball))]
public class BallDragLaunch : MonoBehaviour {


    private Ball ball;


    private float startTime;
    private float endTime;
    

    [Tooltip ("Minimum X value for clamping the ball on the lane")]
    public float minNavPosX;
    [Tooltip("Maximum X value for clamping the ball on the lane")]
    public float maxNavPosX;
  

    private Vector3 dragStart, dragEnd;

	void Start () {
        ball = GetComponent<Ball>();
	}



    /**
     * Called from mouseevents
     * */
    public void DragStart() 
    {
        if (!ball.inPlay)
        {
            startTime = Time.time;
            dragStart = Input.mousePosition;
        }
    }

    public void DragEnd()
    {
        if (!ball.inPlay)
        {
            dragEnd = Input.mousePosition;

            endTime = Time.time;
            float dragDuration = (endTime - startTime);
            float dragDifferenceX = (dragEnd.x - dragStart.x);
            float dragDifferenceZ = (dragEnd.y - dragStart.y);

            float launchSpeedX = dragDifferenceX / dragDuration;
            float launchSpeedZ = dragDifferenceZ / dragDuration;



            Vector3 launchVelocity = new Vector3(launchSpeedX,
                                                 0f,
                                                 launchSpeedZ*1.5f);
            ball.LaunchBall(launchVelocity);
            EnableArrowButtons(false); // just for telling the user why it is not working!
                                       // launch ball
        }
    }

    public void EnableArrowButtons(bool status)
    {
        GameObject[] arrowObjects = GameObject.FindGameObjectsWithTag("ArrowButton");
        foreach (GameObject arrowObject in arrowObjects)
        {
            Button arrowButton = (Button)arrowObject.GetComponent<Button>();
            arrowButton.enabled = status;
        }
    }

    public void MoveStart(float xNudge)
    {
        if (!ball.inPlay)
        {
            
            float clampedXNavPos = Mathf.Clamp(ball.transform.position.x, minNavPosX, maxNavPosX);

            ball.transform.position = new Vector3((clampedXNavPos + xNudge),
                                                   ball.transform.position.y,
                                                   ball.transform.position.z);
        }
    }
}
