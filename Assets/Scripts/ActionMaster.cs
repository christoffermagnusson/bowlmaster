﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMaster {

    
    private int[] pinsDown = new int[21];
    private int bowl = 1;

    public enum Action
    {
        Tidy,
        Reset,
        EndTurn,
        EndGame,
        Default
    }

	public Action Bowl(int pins) // add throw nr, to check strikes, spares etc..
    {
        if(pins < 0 || pins > 10) { throw new UnityException("Invalid number of pins passed"); }

        pinsDown[bowl - 1] = pins;

        // Handle last frame special cases
        switch (bowl)
        {
            case 21:
                return Action.EndGame;


            case 20:
                if (!Bowl21Awarded())
                {
                    return Action.EndGame;
                }else if(Bowl19WasStrike()&& pins != 10)
                {
                    bowl += 1;
                    return Action.Tidy;
                } else if (Bowl21Awarded())
                {
                    bowl += 1;
                    return Action.Reset;
                }
                break;

            case 19:
                if (Bowl21Awarded())
                {
                    bowl += 1;
                    return Action.Reset;
                } break;        
        }
     


        // handle regular cases
        if (bowl % 2 == 0) // end of turn
        {
            Debug.Log("Bowl " + bowl + " triggered endturn");
            bowl += 1;
            return Action.EndTurn;
            
        } else if(bowl % 2 != 0) // midframe 
        {
            if (pins == 10)
            {
                
                bowl += 2;
                Debug.Log("Strike! Skipping a bowl!, new current bowl: "+bowl);
                return Action.EndTurn; // strike
            }
            else
            {
                Debug.Log("Bowl " + bowl+" triggered tidy");
                bowl += 1;
                return Action.Tidy;
            }
        }

        
        
        
        return Action.Default;
    }

   

    private bool Bowl21Awarded()
    {
        if (pinsDown[19-1] == 10)
        {
            return true;
        }
        else if ((pinsDown[19-1] + pinsDown[20-1]) == 10)
        {
            return true;
        }
        return false;
    }

    private bool Bowl19WasStrike()
    {
        if (pinsDown[19 - 1]==10)
        {
            return true;
        }
        return false;
    }

    
}
