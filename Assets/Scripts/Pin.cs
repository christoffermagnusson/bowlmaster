﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour {

    public float standingThresholdX;
    public float standingThresholdZ;

    public float distToRaise = 40f;

    public float standingThreshold;

    private float pinRotationX;
    private float pinRotationZ;

    private bool isStandingControl; // control variable if pin has fallen down
    private Rigidbody rigidBody;

	// Use this for initialization
	void Start () {
        isStandingControl = true;
        rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
       
        pinRotationX = transform.rotation.x; // could be using EulerAngles here.. 
        pinRotationZ = transform.rotation.z; // check back here if this causes any troubles

        

        if (isStandingControl==true)
        {
            
            if (!IsStanding())
            {
                
                isStandingControl = false;
                
            }
            else
            {
               
            }
        }
        
        
	}


    /*
     * Not using EulerAngles function. Inspector says e.g 45 degrees in X, but 
     * with rotation 45 absolute degrees is translated to 0.45 degrees
     * (Use eulerAngles to translate to more sensible numbers
     * 
     * -45f in X rotation considering -90f is the default value
     * */
    public bool IsStanding()
    {

        if (pinRotationX < -45f+standingThresholdX || pinRotationX > -standingThresholdX || pinRotationZ < -standingThresholdZ || pinRotationZ > standingThresholdZ)
        {
            //Debug.Log(name + " is not standing");
            //isStandingControl = false;
            return false;
        }
        return true;

        /*float tiltX = (transform.eulerAngles.x < 180f) ? transform.eulerAngles.x : 360 - transform.eulerAngles.x;
        tiltX -= 90f;
        float tiltZ = (transform.eulerAngles.z < 180f) ? transform.eulerAngles.z : 360 - transform.eulerAngles.z;
        Debug.Log(name + " : X tilt : " + tiltX + " : Z tilt : " + tiltZ);

        if (tiltX > standingThreshold || tiltZ > standingThreshold) { return false; }

        return true;*/
    }

    public void Raise()
    {
        
        transform.rotation = Quaternion.Euler(-90f, 0f, 0f);
            transform.Translate(0f, distToRaise, 0f, Space.World); // raises the pins by distToRaise
            
            
            rigidBody.isKinematic = true;
        
    }

    public void Lower()
    {
        transform.Translate(0f, -(distToRaise+9f), 0f, Space.World); // lower pins by distToRaise + some padding

        rigidBody.isKinematic = false;
        
        
    }
}
