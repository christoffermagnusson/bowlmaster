﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreMaster {

    private SortedDictionary<int,int> frameScores = new SortedDictionary<int,int>();
    

    private int currentScoreInFrame;
    private int currentFrameToScoreIndex = 1;
    private int currentFrameIndex = 1;
    private List<int> frameScoreList = new List<int>();
    private List<int> cumulativeScoreList = new List<int>();

    public enum State
    {
        Strike,
        Spare,
        PointThrow,
        Undefined
    }

    /*
     * Control method that indicates and decides whether this assortment of scores can be handled and scored appropiately 
     * e.g when it is possible to deal out scores for a specific frame.
     * */
    public bool IsFrameScoringPossible(SortedDictionary<int, int[]> frames)
    {
        State state;
        bool isPossible = false; 
        int index = 1;
        foreach(int[] scores in frames.Values)
        {
            state = DecideState(scores);


            switch (state)
            {
                case State.PointThrow:
                    isPossible = true;
                    break;
                case State.Undefined:
                    if (index > 1 && DecideState(frames[index - 1]) == State.Spare) // SCORING AFTER SPARE
                    {
                        isPossible = true;
                    }
                    else if (index > 2 && DecideState(frames[index - 1]) == State.Strike && DecideState(frames[index - 2]) == State.Strike) // 2 CONSECUTIVE STRIKES
                    {
                        isPossible = true;
                    }
                    else
                    {
                        isPossible = false;
                    }
                    break;
                case State.Strike:
                    if (index > 1 && DecideState(frames[index - 1]) == State.Spare) // STRIKE AFTER SPARE
                    {
                        isPossible = true;
                    }else if(index > 2 && DecideState(frames[index-1])== State.Strike && DecideState(frames[index-2])== State.Strike) // 3 CONSECUTIVE STRIKES
                    {
                        isPossible = true;
                    }
                    else
                    {
                        isPossible = false;
                    }
                    break;
                case State.Spare:
                    if(index > 1 && DecideState(frames[index-1])== State.Strike)
                    {
                        isPossible = true;
                    }
                    break;
            }
            index++;
        }
        return isPossible;
    }

    public List<int> CalculateFrameScore(SortedDictionary<int, int[]> frames, int startingFrame) // change so it starts from currentFrameToScore
    {
        currentFrameIndex = currentFrameToScoreIndex; // stupid allocation .. 
        
        
            try
            {
                State state = DecideState(frames[currentFrameIndex]);
                switch (state)
                {
                    case State.Strike:
                        if (DecideState(frames[currentFrameIndex + 1]) != State.Strike)
                        {
                            currentScoreInFrame = 10 + (frames[currentFrameIndex + 1][0] + frames[currentFrameIndex + 1][1]);
                            Debug.Log("Getting here after strike! With score " + currentScoreInFrame);
                        }
                        else
                        {
                            currentScoreInFrame = 20 + frames[currentFrameIndex + 2][0];
                            Debug.Log("Getting here after consecutive strikes! With score " + currentScoreInFrame);
                        }
                        break;
                    case State.Spare:
                        Debug.Log("Getting here after spare! With score " + (10 + frames[currentFrameIndex + 1][0]));
                        currentScoreInFrame = 10 + frames[currentFrameIndex + 1][0];
                        break;
                    case State.PointThrow:
                        currentScoreInFrame = (frames[currentFrameIndex][0] + frames[currentFrameIndex][1]);
                        Debug.Log("Getting here after regular framescoring! With score " + currentScoreInFrame);
                        break;
                    case State.Undefined:
                        break;
                }
                AddFrameScore();
                
                currentScoreInFrame = 0;
            }
            

            catch (IndexOutOfRangeException) // catch if set is not complete
            {
                Debug.Log("Round not finished yet! Waiting for more scores!");
            }
            catch (KeyNotFoundException)
            {
                Debug.Log("Round not finished yet! Waiting for more scores!");
            }
        

        
        
        
        frameScoreList = ConvertToList(frameScores);
        return frameScoreList;
    }

    

    /*
     * When scores have been calculated they are added to the frame to be scored in this method. 
     * After added the index is incremented to indicate that this frame has been handled
     * */
    private void AddFrameScore()
    {
        if (frameScores.Count < currentFrameToScoreIndex)
        {
            if (!frameScores.ContainsKey(currentFrameToScoreIndex))
            {
                frameScores.Add(currentFrameToScoreIndex, currentScoreInFrame);
                Debug.Log("Adding scores " + currentScoreInFrame + " to index : " + currentFrameToScoreIndex);
            }
          
            currentFrameToScoreIndex++;
        }
    }

    public int GetCurrentFrameToScore()
    {
        return currentFrameToScoreIndex;
    }

    public int GetRunningTotal()
    {
        int runningTotal = 0;
        foreach(int frameScore in frameScoreList)
        {
            runningTotal += frameScore; 
        }
        return runningTotal;
    }


    private List<int> ConvertToList(SortedDictionary<int,int> toBeConverted)
    {
        List<int> toBeReturned = new List<int>();
        foreach (int frameScore in toBeConverted.Values)
        {
            toBeReturned.Add(frameScore);
        }
        return toBeReturned;
    }


    private State DecideState(int[] frameScores)
    {
        try
        {
            if (frameScores[0] == 10)
            {
                return State.Strike;
            }
            else if (frameScores[0] + frameScores[1] == 10)
            {
                return State.Spare;
            }
            else if (frameScores[0] + frameScores[1] != 10)
            {
                return State.PointThrow;
            }

            if (frameScores == null)
            {
                return State.Undefined;
            }
        }
        catch (IndexOutOfRangeException)
        {
            return State.Undefined;
        }
        return State.Undefined;
        
    }
}
