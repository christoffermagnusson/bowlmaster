﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneBox : MonoBehaviour {

    PinCounter pinCounter;

    private void Start()
    {
        pinCounter = GameObject.FindObjectOfType<PinCounter>();
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Ball")
        {
            
            pinCounter.ballLeftBox = true;
        }
    }

}
