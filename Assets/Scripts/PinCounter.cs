﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinCounter : MonoBehaviour {

    public bool ballLeftBox = false;
    public int lastStandingCount = -1;
    private int lastSettledCount = 10;

    
    
    public enum TextColor
    {
        Default,
        Red,
        Green
    }

    private Text pinCounter;
    private float lastChangeTime;

    private Color defaultColor = new Color(255, 125, 24);

    private GameManager gameManager;
    private ArrayList standingPinArray = new ArrayList(); // holder of the Pin (Scripts, classes)


    // Use this for initialization
    void Start () {
        GameObject pinCounterObject = GameObject.Find("Pin Counter");
        pinCounter = pinCounterObject.GetComponent<Text>();

        UpdatePinCounterText(TextColor.Default);

        gameManager = GameObject.FindObjectOfType<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
        if (ballLeftBox)
        {
            UpdatePinCounterText(TextColor.Red);
            CheckStanding();

        }
    }


   

    private void CheckStanding() // runs every update to see if pins have settled..
    {
        int currentStanding = CountStanding();


        if (currentStanding != lastStandingCount)
        {
            lastChangeTime = Time.time;
            lastStandingCount = currentStanding;
            return;
        }
        float settleTime = 3f; // How long to wait to consider pins settled in seconds

        if ((Time.time - lastChangeTime) > settleTime) // if last change was more than 3 seconds ago
        {
            PinsHaveSettled();// Pins have settled
        }


    }


    /**
    * Determining method to enable a fresh gamestate. 
    * Here because the pinsetter knows about pins
    * */
    private void PinsHaveSettled()
    {
        int standing = CountStanding();
        int pinsFallen = lastSettledCount - standing;
        lastSettledCount = standing; // set to current standing, resets at end turn ex.
        lastStandingCount = -1; // indicates a refresh of gamestate
        UpdatePinCounterText(TextColor.Green);
        ballLeftBox = false;
        
        gameManager.ReceiveFallenPins(pinsFallen);
        
        

    }

    /*
    * Method finds all GameObjects with the "Pin" tag
    * and gets the Pin script component on each one. 
    * Then check with the script if the current
    * pin is standing and has not yet been added to the Pin array, 
    * and if that is the case, then add it to the standing Pin array. 
    * Else if the pin is not standing, and is added to the standing
    * Pin array, remove it from the array
    * 
    * @ returns the count of the standingPinArray
    * */
    private int CountStanding()
    {
        Pin[] pins = FindObjectsOfType<Pin>();


        foreach (Pin pin in pins)
        {

            if (pin.IsStanding() && !standingPinArray.Contains(pin))
            {
                standingPinArray.Add(pin);

            }
            else if (!pin.IsStanding() && standingPinArray.Contains(pin))
            {

                standingPinArray.Remove(pin);
            }
            else if (pin == null)
            {
                standingPinArray.Remove(pin);
            }
        }
        return standingPinArray.Count;
    }

    /**
     * Making use of some metaphor for simplicity
     * */
    public void ClearPinArray()
    {
        
        standingPinArray.Clear();
        
    }

    public ArrayList GetStandingPinArray()
    {
        return standingPinArray;
    }


    /**
    * Updating the displayed text to make sure the player has 
    * the score at all times.. might be refactored to a Score-class?
    * */

    public void UpdatePinCounterText(TextColor color)

    {
        switch (color)
        {
            case TextColor.Default:
                pinCounter.color = defaultColor;
                break;
            case TextColor.Red:
                pinCounter.color = Color.red;
                break;
            case TextColor.Green:
                pinCounter.color = Color.green;
                break;

        }
        pinCounter.text = "" + lastSettledCount;
        
    }

    public void Reset()
    {
        lastSettledCount = 10;
        ClearPinArray();
    }
}
