﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shredder : MonoBehaviour {

    private void OnTriggerExit(Collider other)
    {
        GameObject collider = other.gameObject;
        if (collider.GetComponent<Pin>())
        {
            
            Destroy(collider);

        }

    }
}
