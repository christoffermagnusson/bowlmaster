﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    

    private Vector3 offset;
    private Vector3 resetPosition;


    public Ball ball;
    

	// Use this for initialization
	void Start () {
        offset = transform.position - ball.transform.position;
        resetPosition = transform.position;
	}

    public void Reset()
    {
        transform.position = resetPosition;        
    }

    // Update is called once per frame
    void Update () {
        if (transform.position.z < 1700f) // in front of headpin follow ball, after 1829, stop camera moving
            {
            transform.position = ball.transform.position + offset;
            }
        }
}
