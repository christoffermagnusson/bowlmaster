﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;

[TestFixture]
public class ScoreDisplayTest{

    private ScoreDisplay scoreDisplay;
    private List<int> individScoreList;

    [SetUp]
    public void SetUp()
    {
        scoreDisplay = GameObject.FindObjectOfType<ScoreDisplay>();
        scoreDisplay.InitializeDisplay();
        individScoreList = new List<int>();
    }

    [Test]
    public void T00_FailingTest()
    {
        bool failing = true;
        Assert.That(failing);
    }

    [Test]
    public void T01_Score0and0Displays0and0()
    {
        string[] testResults = { "0", "0" };
        individScoreList.Add(0);
        individScoreList.Add(0);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(1, individScoreList));
    }

    [Test]
    public void T02_Score1and5Display1and5()
    {
        string[] testResults = { "1", "5" };
        individScoreList.Add(1);
        individScoreList.Add(5);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(1, individScoreList));
    }

    [Test]
    public void T03_Score8and2Display8andSpare()
    {
        string[] testResults = { "8", "/" };
        individScoreList.Add(8);
        individScoreList.Add(2);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(1, individScoreList));

    }

    [Test]
    public void T04_ScoreStrikeDisplayXandNill()
    {
        string[] testResults = { "X", "-" };
        individScoreList.Add(10);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(1, individScoreList));

    }

    [Test]
    public void T05_Score5SpareInFrame10Display5Spare()
    {
        string[] testResults = { "5", "/", "0" };
        individScoreList.Add(5);
        individScoreList.Add(5);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(10, individScoreList));
    }

    [Test]
    public void T06_ScoreStrikeInFirstBowlInFrame10DisplayX00()
    {
        string[] testResults = { "X", "0", "0" };
        individScoreList.Add(10);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(10, individScoreList));
    }
    [Test]
    public void T07_Score2FirstStrikesInFrame10DisplayXX0()
    {
        string[] testResults = { "X", "X", "0" };
        individScoreList.Add(10);
        individScoreList.Add(10);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(10, individScoreList));
    }

    [Test]
    public void T08_Score3StrikesInFrame10DisplayXXX()
    {
        string[] testResults = { "X", "X", "X" };
        individScoreList.Add(10);
        individScoreList.Add(10);
        individScoreList.Add(10);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(10, individScoreList));
    }

    [Test]
    public void T09_ScoreStrikeInFrame10Bowl1AndSpareIn3DisplayX5Spare()
    {
        string[] testResults = { "X", "5", "/" };
        individScoreList.Add(10);
        individScoreList.Add(5);
        individScoreList.Add(5);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(10, individScoreList));
    }

    [Test]
    public void T10_ScorePointThrowInFrame10Display540()
    {
        string[] testResults = { "5", "4", "0" };
        individScoreList.Add(5);
        individScoreList.Add(4);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(10, individScoreList));
    }

    [Test]
    public void T10_SpareInSecondBowlInFrame10Display5Spare0()
    {
        string[] testResults = { "5", "/", "0" };
        individScoreList.Add(5);
        individScoreList.Add(5);
        Assert.AreEqual(testResults, scoreDisplay.TestingBowls(10, individScoreList));
    }


}
