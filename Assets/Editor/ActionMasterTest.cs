﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

[TestFixture]
public class ActionMasterTest {

    private bool failing;

    private ActionMaster.Action endTurn = ActionMaster.Action.EndTurn;
    private ActionMaster.Action tidy = ActionMaster.Action.Tidy;
    private ActionMaster.Action endGame = ActionMaster.Action.EndGame;
    private ActionMaster.Action reset = ActionMaster.Action.Reset;

    private ActionMaster actionMaster;

    private int [] rolls = { 10, 0, 4, 5, 2, 8, 1, 1, 3, 0, 0, 10, 1, 2, 3, 6, 7, 2, 9, 1, 7 };

    [SetUp]
    public void SetUp()
    {
        actionMaster = new ActionMaster();
    }

	[Test]
	public void T00_PassingTest() {
        failing = true;
        Assert.IsTrue(failing);
	}

    [Test]
    public void T01_StrikeReturnsEndTurn()
    {
        Assert.AreEqual(endTurn, actionMaster.Bowl(10));
    }

    [Test]
    public void T02_Bowl5FirstOfFrameReturnsTidy()
    {
        
        Assert.AreEqual(tidy, actionMaster.Bowl(5));   
    }

    [Test]
    public void T03_LastFrameReturnsEndGame()
    {
        
        for(int i=0; i<rolls.Length; i++)
        {
            if (i < 20)
            {
                actionMaster.Bowl(rolls[i]);
            }
        }
        Assert.AreEqual(endGame, actionMaster.Bowl(6));
        
    }

    [Test]
    public void T04_Bowl_28SpareReturnsEndTurn()
    {
        actionMaster.Bowl(2);
        Assert.AreEqual(endTurn, actionMaster.Bowl(8));
    }

    [Test]
    public void T05_EvenNumberedBowlsWithoutSpareReturnsEndTurn()
    {
        actionMaster.Bowl(3);
        Assert.AreEqual(endTurn, actionMaster.Bowl(2));
    }

    [Test]
    public void T06_NoStrikeInBowl19ReturnsTidy()
    {
        
        for(int i=0; i<18; i++)
        {
            
            if (! EnsureBowlNotThrownAfterStrikeUnder19(i)) // if a bowl is of value 0 then check if previous bowl is a strike 
            {
                
                actionMaster.Bowl(rolls[i]);
               
            }
        }
        Assert.AreEqual(tidy, actionMaster.Bowl(2));
    }

    [Test]
    public void T07_StrikeInBowl19ReturnsReset()
    {
        
        for (int i=0; i<18; i++)
        {
            if (!EnsureBowlNotThrownAfterStrikeUnder19(i)) // if a bowl is of value 0 then check if previous bowl is a strike 
            {
                actionMaster.Bowl(rolls[i]);
                

            }
        }
        Assert.AreEqual(reset, actionMaster.Bowl(10));
    }

    [Test]
    public void T08_SpareInBowl20ReturnsReset()
    {
        int[] rolls = { 1, 2, 3, 4, 5, 4, 3, 2, 1, 2, 3, 4, 5, 5, 4, 3, 2, 1 };
        foreach (int roll in rolls)
        {
            actionMaster.Bowl(roll);
        }
        Assert.AreEqual(tidy,actionMaster.Bowl(8));
        Assert.AreEqual(reset, actionMaster.Bowl(2));
    }

    [Test]
    public void T09_Bowl20WithoutSpareReturnsEndGame()
    {
        int[] rolls = { 1, 2, 3, 4, 5, 4, 3, 2, 1, 2, 3, 4, 5, 5, 4, 3, 2, 1 };
        foreach (int roll in rolls)
        {
            actionMaster.Bowl(roll);
        }
        Assert.AreEqual(tidy, actionMaster.Bowl(2));
        Assert.AreEqual(endGame, actionMaster.Bowl(5));
    }

    /*[Test]
    public void T10_EvenNumberedBowlsUnder20ReturnsEndTurn()
    {
         for(int i=0; i<20; i++)
        {
            if (!(rolls[i] == 0 && rolls[i - 1] == 10)) {
                if ((i + 1) % 2 == 0)
                {
                    Assert.AreEqual(endTurn, actionMaster.Bowl(rolls[i]));
                    Debug.Log("Asserted bowl :" + (i + 1) + " as endTurn");
                }
                actionMaster.Bowl(rolls[i]);
            }
        }
    }*/

    [Test]
    public void T11_Bowl20ReturnsTidyAfterStrikeInBowl19()
    {
        for(int i=0; i<18; i++)
        {
            if(!EnsureBowlNotThrownAfterStrikeUnder19(i))
            {
                actionMaster.Bowl(rolls[i]);
                Debug.Log("index : " + (i+1));
            }
        }
        actionMaster.Bowl(10); // strike in 19
        Assert.AreEqual(tidy,actionMaster.Bowl(2)); // regular in 20 should return tidy
    }

    [Test]
    public void T12_Bowl0in20ReturnsTidyAfterStrikeInBowl19()
    {
        for (int i = 0; i < 18; i++)
        {
            if (!EnsureBowlNotThrownAfterStrikeUnder19(i))
            {
                actionMaster.Bowl(rolls[i]);
                Debug.Log("index : " + (i + 1));
            }
        }
        actionMaster.Bowl(10); // strike in 19
        Assert.AreEqual(tidy, actionMaster.Bowl(0)); // regular in 20 should return tidy
    }

    [Test]
    public void T13_InvalidBowlsReturnsUnityException()
    {
        Assert.That(() => actionMaster.Bowl(-1), Throws.Exception);
        Assert.That(() => actionMaster.Bowl(11), Throws.Exception);
    }

    [Test]
    public void T14_3StrikesInLastFrameReturns2Reset1EndGame()
    {
        for(int i=0; i<18; i++)
        {
            if(!EnsureBowlNotThrownAfterStrikeUnder19(i))
            {
                actionMaster.Bowl(rolls[i]);
            }
        }
        Assert.AreEqual(reset,actionMaster.Bowl(10));
        Assert.AreEqual(reset, actionMaster.Bowl(10));
        Assert.AreEqual(endGame, actionMaster.Bowl(10));
    }


    private bool EnsureBowlNotThrownAfterStrikeUnder19(int index)
    {
        if(rolls[index] == 0 && rolls[index - 1] == 10)
        {
            return true;
        }
        return false;
    }









    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
	public IEnumerator TestWithEnumeratorPasses() {
		// Use the Assert class to test conditions.
		// yield to skip a frame
		yield return null;
	}
}
