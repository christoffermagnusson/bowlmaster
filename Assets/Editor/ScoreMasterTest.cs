﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;

[TestFixture]
public class ScoreMasterTest {


    private ScoreMaster scoreMaster;

    SortedDictionary<int, int[]> scores = new SortedDictionary<int, int[]>();
    List<int> returnedScores;

    [SetUp]
    public void SetUp()
    {
        scoreMaster = new ScoreMaster();
    }

    [Test]
    public void T01_FailingTest()
    {
        bool initialFail = true;
        Assert.IsTrue(initialFail);
    }

    [Test]
    public void T02_Score54InFrame1ReturnsScore9()
    {
        scores.Clear();
        int[] frame1Score = { 5, 4 };
        scores.Add(1,frame1Score);
        returnedScores = scoreMaster.CalculateFrameScore(scores,1);
        Assert.AreEqual(9, returnedScores[0]);
        
    }

    [Test]
    public void T03_Score54AfterStrikeInFirstFrameReturns19InFrame1()
    {
        scores.Clear();
        int[] frame1Score = { 10, 0 };
        int[] frame2Score = { 5, 4 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        returnedScores = scoreMaster.CalculateFrameScore(scores,1);
        Assert.AreEqual(19, returnedScores[0]);
        
    }

    [Test]
    public void T04_Score43AfterStrikeReturns17InFrame1And7InFrame2()
    {
        scores.Clear();
        int[] frame1Score = { 10, 0 };
        int[] frame2Score = { 4 ,3};
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(17, returnedScores[0]);
        
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(7, returnedScores[1]);

    }

    [Test]
    public void T05_Score8AfterSpareReturns18InFrame1()
    {
        scores.Clear();
        int[] frame1Score = { 8, 2 };
        int[] frame2Score = { 8 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        returnedScores = scoreMaster.CalculateFrameScore(scores,1);
        Assert.AreEqual(18, returnedScores[0]);
        
    }

    [Test]
    public void T06_Score62After2StrikesReturns26InFirstFrameAnd18InSecondFrame()
    {
        scores.Clear();
        int[] frame1Score = { 10, 0 };
        int[] frame2Score = { 10, 0 };
        int[] frame3Score = { 6, 2 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        scores.Add(3, frame3Score);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(26, returnedScores[0]);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(18, returnedScores[1]);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(8, returnedScores[2]);

    }

    [Test]
    public void T07_ScoreStrikeAfterSpareReturns20InFirstFrame()
    {
        scores.Clear();
        int[] frame1Score = { 2, 8 };
        int[] frame2Score = { 10, 0 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        returnedScores = scoreMaster.CalculateFrameScore(scores,1);
        Assert.AreEqual(20, returnedScores[0]);
    }

    [Test]
    public void T08_Score2StrikesPlus54AfterSpareReturns20InFirstFrame25Second24ThirdAnd9InFourth()
    {
        scores.Clear();
        int[] frame1Score = { 2, 8 };
        int[] frame2Score = { 10, 0 };
        int[] frame3Score = { 10, 0 };
        int[] frame4Score = { 5, 4 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        scores.Add(3, frame3Score);
        scores.Add(4, frame4Score);
     
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(20, returnedScores[0]);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(25, returnedScores[1]);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(19, returnedScores[2]);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        Assert.AreEqual(9, returnedScores[3]);
        
    }

    [Test]
    public void T09_CumulativeScoreReturnsAs73WhenScoreAsT08()
    {
        scores.Clear();
        int[] frame1Score = { 2, 8 };
        int[] frame2Score = { 10, 0 };
        int[] frame3Score = { 10, 0 };
        int[] frame4Score = { 5, 4 };
        scores.Add(1, frame1Score);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        scores.Add(2, frame2Score);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        scores.Add(3, frame3Score);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }
        scores.Add(4, frame4Score);
        if (scoreMaster.IsFrameScoringPossible(scores))
        {
            returnedScores = scoreMaster.CalculateFrameScore(scores, 1);
        }

        Assert.AreEqual(73, scoreMaster.GetRunningTotal());
    }

    [Test]
    public void T10_3StrikesReturns30InFirstFrame()
    {
        scores.Clear();
        int[] frame1Score = { 10, 0 };
        int[] frame2Score = { 10, 0 };
        int[] frame3Score = { 10, 0 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        scores.Add(3, frame3Score);
        returnedScores = scoreMaster.CalculateFrameScore(scores,1);
        Assert.AreEqual(30, returnedScores[0]);
       
    }

    [Test]
    public void T11_PointThrowIsEligebleForScoring()
    {
        scores.Clear();
        int[] frame1Score = { 1, 1 };
        scores.Add(1, frame1Score);
        Assert.IsTrue(scoreMaster.IsFrameScoringPossible(scores));

    }

    [Test]
    public void T12_UndefinedAfterSpareIsEligebleForScoring()
    {
        scores.Clear();
        int[] frame1Score = { 1, 9 };
        int[] frame2Score = { 2 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        Assert.IsTrue(scoreMaster.IsFrameScoringPossible(scores));

    }

    [Test]
    public void T13_UndefinedAfter2StrikesIsEligebleForScoring()
    {
        scores.Clear();
        int[] frame1Score = { 10, 0 };
        int[] frame2Score = { 10, 0 };
        int[] frame3Score = { 2 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        scores.Add(3, frame3Score);
        Assert.IsTrue(scoreMaster.IsFrameScoringPossible(scores));

    }

    [Test]
    public void T13_SingleStrikeIsNotEligebleForScoringAfterPointThrow()
    {
        scores.Clear();
        int[] frame1Score = { 10, 0 };
        scores.Add(1, frame1Score);
        Assert.IsFalse(scoreMaster.IsFrameScoringPossible(scores));

    }

    [Test]
    public void T14_DoubleStrikeIsNotEligebleForScoringAfterPointThrow()
    {
        scores.Clear();
        int[] frame1Score = { 1, 1 };
        int[] frame2Score = { 10, 0 };
        int[] frame3Score = { 10, 0 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        scores.Add(3, frame3Score);
        Assert.IsFalse(scoreMaster.IsFrameScoringPossible(scores));

    }

    [Test]
    public void T14_SingleStrikeIsEligebleForScoringAfterSpare()
    {
        scores.Clear();

        int[] frame1Score = { 8, 2 };
        int[] frame2Score = { 10, 0 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        Assert.IsTrue(scoreMaster.IsFrameScoringPossible(scores));

    }

    [Test]
    public void T15_3StrikesIsEligebleForScoring()
    {
        scores.Clear();

        int[] frame1Score = { 10, 0 };
        int[] frame2Score = { 10, 0 };
        int[] frame3Score = { 10, 0 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        scores.Add(3, frame3Score);
        Assert.IsTrue(scoreMaster.IsFrameScoringPossible(scores));

    }

    [Test]
    public void T16_SpareAfterStrikeIsEligebleForScoring()
    {
        scores.Clear();

        int[] frame1Score = { 10, 0 };
        int[] frame2Score = { 8, 2 };
        //int[] frame3Score = { 10, 0 };
        scores.Add(1, frame1Score);
        scores.Add(2, frame2Score);
        //scores.Add(3, frame3Score);
        Assert.IsTrue(scoreMaster.IsFrameScoringPossible(scores));

    }



}
